FROM "ubuntu:22.04"

RUN apt-get update && \
    apt-get install -y make \
    python3-pip \
    bash \
    twine \
    git \
    curl \
    lsb-core

# Install docker
RUN mkdir -p /etc/apt/keyrings && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
RUN echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN apt-get update && \
    apt-get install -y docker-ce \    
    docker-ce-cli 

# Install poetry 
RUN pip install poetry

